/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Lazar
 */
public interface DomainObject {
    
      String getColumnNamesForInsert();
    
     String getColumnValuesForInsert();
    
     String getColumnValuesForUpdate();

     String getTableName();

     String getWhereCondition();
    
     DomainObject getNewRecord(ResultSet rs) throws SQLException;
    
     boolean isIdAutoincrement();

     void setAutoincrementId(Long id);
}
