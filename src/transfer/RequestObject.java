/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transfer;

import java.io.Serializable;

/**
 *
 * @author Lazar
 */
public class RequestObject implements Serializable{
    
    private String operacija;
    private Object data;

    public RequestObject(String operacija, Object data) {
        this.operacija = operacija;
        this.data = data;
    }

    public RequestObject() {
    }

    public RequestObject(String operacija) {
        this.operacija = operacija;
    }
    
    public String getOperacija() {
        return operacija;
    }

    public void setOperacija(String operacija) {
        this.operacija = operacija;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
    
    
}
